
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World II
///
/// This program has both Object Oriented and procedural elements in it
///
/// @file hello2.cpp
/// @version 1.0
//Christopher Aguilar
///
/// Feb 2021
//

#include <iostream>


int main() {

   std::cout << "Hello" << " " << "World!" << std::endl;

   return 0;

}

