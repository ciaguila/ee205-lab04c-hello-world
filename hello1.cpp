
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World II
///
/// This program has both Object Oriented and procedural elements in it
///
/// @file hello1.cpp
/// @version 1.0
//Christopher AGuilar
///
/// Feb 2021
//

#include <iostream>

using namespace std;

int main() {

    cout << "Hello" << " " << "World!" << endl; 

   return 0;

}

